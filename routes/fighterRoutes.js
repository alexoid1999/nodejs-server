const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

const registerFighter = (req,res,next)=>{
    try{
        // Выполняем проверку со всеми бойца из базы
        let activeFighters = FighterService.getAll();
        // Если база не пустая на данный момент
        if(activeFighters!==null && activeFighters.length>0){
            // Сравниваем реквест с каждым бойцом из базы
            let possibleMatches = activeFighters.map(activeFighter=>{
                // Если имена совпадают, то возвращаем ошибку 
                if(activeFighter.name.toLowerCase()==req.body.name.toLowerCase()){
                    return `${activeFighter.name} already exist`;
                }
                // Иначе возвращаем ответ-пустышку
                else return 'No matches'
            // И отфильтровуем пустышки
            }).filter(elem => elem!=='No matches');
            // И если после отсеивания пыстышек массив возможных совпадений не пуст,
            // то возвращаем ошибку
            if(possibleMatches.length>0){res.err = `Can't register: ${possibleMatches.toString()}`}
            // Иначе - создаём нового бойца
            else{
                let savedFighter = FighterService.create(req.body);
                if(savedFighter!==null){
                    res.data = savedFighter;
                }
                else {
                    res.err = `Can't save fighter ${req.body.name}`;
                }
            }
        }
        // Если база ещё пустая, то в любом случае создаём бойца
        else{
            let savedFighter = FighterService.create(req.body);
            if(savedFighter!==null){
                res.data = savedFighter;
            }
            else {
                res.err = `Can't save fighter ${req.body.name}`;
            }
        }
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}

const getFighters = (req,res,next)=>{
    try{
        let dbFighters = FighterService.getAll();
        if(dbFighters) res.data = dbFighters
        else res.notFound = "Can't get all fighters"
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}

const getFighterById = (req, res, next)=>{
    try{
        let dbFighter = FighterService.search({ id: req.params.id});
        if(dbFighter) res.data = dbFighter
        else res.notFound = `Can't get fighter by id ${req.params.id}`
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}

const updateFighterById = (req, res, next)=>{
    try{
        let updateFighter = FighterService.update(req.params.id, req.body);
        if(updateFighter) res.data = updateFighter
        else {res.notFound = `Can't update fighter by id ${req.params.id}`}
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}

const deleteFighterById = (req,res, next)=>{
    try{
        delete res.err;
        let deleteFighter = FighterService.delete(req.params.id);
        if(deleteFighter) {res.data = deleteFighter}
        else {res.notFound = `Can't delete figther by id ${req.params.id}`}
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}


router.get('/', getFighters, responseMiddleware);
router.get('/:id', getFighterById, responseMiddleware);
router.post('/', createFighterValid, responseMiddleware, registerFighter, responseMiddleware);
router.put('/:id', updateFighterValid, responseMiddleware, updateFighterById, responseMiddleware);
router.delete('/:id', deleteFighterById, responseMiddleware)
// TODO: Implement route controllers for fighter

module.exports = router;