const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

const registerUser = (req,res,next)=>{
    try{
        // Выполняем проверку со всеми юзерами из базы
        let activeUsers = UserService.getAll();
        // Если база не пустая на данный момент
        if(activeUsers!==null && activeUsers.length>0){
            // Сравниваем реквест с каждым юзером из базы
            let possibleMatches = activeUsers.map(activeUser=>{
                // Если емейлы или номера совпадают, то возвращаем ошибку 
                if(activeUser.email.toLowerCase()==req.body.email.toLowerCase()){
                    return `${activeUser.email} already exist`;
                }
                else if(activeUser.phoneNumber.toLowerCase()==req.body.phoneNumber.toLowerCase()){
                    return `${activeUser.phoneNumber} already exist`;
                }
                // Иначе возвращаем ответ-пустышку
                else return 'No matches'
            // И отфильтровуем пустышки
            }).filter(elem => elem!=='No matches');
            // И если после отсеивания пыстышек массив возможных совпадений не пуст,
            // то возвращаем ошибку
            if(possibleMatches.length>0){res.err = `Can't register: ${possibleMatches.toString()}`}
            // Иначе - создаём нового юзера
            else{
                let savedUser = UserService.create(req.body);
                if(savedUser!==null){
                    res.data = savedUser;
                }
                else {
                    res.err = `Can't save user ${req.body.email}`;
                }
            }
        }
        // Если база ещё пустая, то в любом случае регистрируем
        else{
            let savedUser = UserService.create(req.body);
            if(savedUser!==null){
                res.data = savedUser;
            }
            else {
                res.err = `Can't save user ${req.body.email}`;
            }
        }
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}

const getUsers = (req,res,next)=>{
    try{
        let dbUsers = UserService.getAll();
        if(dbUsers) res.data = dbUsers
        else res.notFound = "Can't get all users"
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}

const getUserById = (req, res, next)=>{
    try{
        let dbUser = UserService.search({ id: req.params.id});
        if(dbUser) res.data = dbUser
        else res.notFound = `Can't get user by id ${req.params.id}`
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}

const updateUserById = (req, res, next)=>{
    try{
        let dbUser = UserService.search({ id: req.params.id});
        if(dbUser) {
            let updateUser = UserService.update(req.params.id, req.body);
            if(updateUser) res.data = updateUser
            else {res.err = `Can't update user by id ${req.params.id}`}
        }
        else res.err = `Can't find user with id ${req.params.id} in database`
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}

const deleteUserById = (req,res, next)=>{
    try{
        delete res.err;
        let deleteUser = UserService.delete(req.params.id);
        if(deleteUser) {res.data = deleteUser}
        else {res.notFound = `Can't delete user by id ${req.params.id}`}
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}


router.get('/', getUsers, responseMiddleware);
router.get('/:id', getUserById, responseMiddleware);
router.post('/', createUserValid, responseMiddleware, registerUser, responseMiddleware);
router.put('/:id', updateUserValid, responseMiddleware, updateUserById, responseMiddleware);
router.delete('/:id', deleteUserById, responseMiddleware)

module.exports = router;