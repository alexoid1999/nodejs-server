const { Router } = require('express');
const FightService = require('../services/fightService');
const UserService = require('../services/userService');
const { createFightValid, updateFightValid } = require('../middlewares/fight.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();
// OPTIONAL TODO: Implement route controller for fights

const getFights = (req,res,next)=>{
    try{
        let dbFights = FightService.getAll();
        if(dbFights) res.data = dbFights
        else res.notFound = "Can't get all fights"
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}

const getFightById = (req,res,next)=>{
    try{
        let dbFight = FightService.search({id: req.params.fightId});
        if(dbFight) res.data = dbFight
        else res.notFound = `Can't find fight with id ${req.body.fightId}`
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}

const createFight = (req,res,next)=>{
    try{
        let savedFight = FightService.create(req.body);
        if(savedFight!==null){
            res.data = savedFight;
        }
        else {
            res.err = `Can't create fight with ${req.body}`;
        }
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}

const updateFight = (req,res,next)=>{
    try{
        let prevStateFight = FightService.search({id: req.body.id});
        if(prevStateFight!==null){
            let newStateFight = FightService.update(req.body.id, {log:[...prevStateFight.log, req.body.log]});
            if(newStateFight!==null){
                res.data = newStateFight;
            }
            else {
                res.err = `Can't update fight with id ${req.body.id}`;
            }
        }
        else {
            res.err = `Can't find fight with id ${req.body.id}`;
        }
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}

const getUserById = (req, res, next)=>{
    try{
        let dbUser = UserService.search({ id: req.params.userId});
        if(!dbUser) res.notFound = `Can't get user by id ${req.params.userId}`
    }
    catch(err){
        console.log(err)
        res.err = "Ooops. Here is a server error";
    }
    finally{
        next();
    }
}

/* 
Get запрос для получения списка боёв
*/
router.get('/', getFights, responseMiddleware);

/* 
Get запрос для получения списка боёв
*/
router.get('/:fightId', getFightById, responseMiddleware);

/* 
Post запрос для инициализации боя (создание учётной записи боя)
Принимает в query айди пользователя, который хочет начать бой
А в body - два айди бойцов, с которыми хотят начать бой
формат body: {fighter1:"id", fighter2:"id"}

Присутствует валидация запроса на создание:
 Чтобы body содержал только поля из модели
 Чтобы каждое поле имело значение, соответствующее регулярному выражению из модели
*/

router.post('/:userId', getUserById, responseMiddleware, createFightValid, responseMiddleware, createFight, responseMiddleware);
/* 
Put запрос для обновления лога боя 
Принимает в query айди пользователя, который хочет обновить бой
А в body - айди боя для обновления лога и сам лог
формат body: {
    id:"id",
    log:{
        fighter1Shot: "number",
        fighter2Shot: "number",
        fighter1Health: "number",
        fighter2Health: "number"
    }
Присутствует валидация запроса на изменение:
 Чтобы body содержал только поля из модели
 Чтобы поле id имело значение, соответствующее регулярному выражению из модели
  Чтобы ключи объекта log соответствовали ключам log из модели
  Чтобы каждое значение поля объекта log соответствовало регулярному выражению из модели log
*/
router.put('/:userId', getUserById, responseMiddleware, updateFightValid, responseMiddleware, updateFight, responseMiddleware)

module.exports = router;