const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    create(fighter) {
        const result = FighterRepository.create(fighter);
        if(!result) {return null}
        else{
            return result;
        }
    }

    update(id, dataToUpdate) {
        const result = FighterRepository.update(id,dataToUpdate);
        if(!result) {return null}
        else{
            return result;
        }
    }

    getAll() {
        const result = FighterRepository.getAll();
        if(!result) {return null}
        else{
            return result;
        }
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {return null}
        else{
            return item;
        }
    }
    delete(id){
        const result = FighterRepository.delete(id);
        if(!result || result.length==0) {return null}
        else{
            console.log(`DELETE Sucess fighter ${id}`);
            console.log(result[0])
            return result[0]
        }
    }
}

module.exports = new FighterService();