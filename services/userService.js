const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    create(user) {
        const result = UserRepository.create(user);
        if(!result) {return null}
        else{
            return result;
        }
    }

    update(id, dataToUpdate) {
        const result = UserRepository.update(id,dataToUpdate);
        if(!result) {return null}
        else{
            return result;
        }
    }

    getAll() {
        const result = UserRepository.getAll();
        if(!result) {return null}
        else{
            return result;
        }
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {return null}
        else{
            return item;
        }
    }
    delete(id){
        const result = UserRepository.delete(id);
        if(!result || result.length==0) {return null}
        else{
            console.log(result[0])
            return result[0]
        }
    }
}

module.exports = new UserService();