const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    // OPTIONAL TODO: Implement methods to work with fights
    create(fight) {
        fight.log=[];
        const result = FightRepository.create(fight);
        if(!result) {return null}
        else{
            return result;
        }
    }

    update(id, dataToUpdate) {
        const result = FightRepository.update(id,dataToUpdate);
        if(!result) {return null}
        else{
            return result;
        }
    }

    getAll() {
        const result = FightRepository.getAll();
        if(!result) {return null}
        else{
            console.log(`GET all fights`);
            return result;
        }
    }

    search(search) {
        const item = FightRepository.getOne(search);
        if(!item) {return null}
        else{
            return item;
        }
    }
    delete(id){
        const result = FightRepository.delete(id);
        if(!result || result.length==0) {return null}
        else{
            console.log(result[0])
            return result[0]
        }
    }
}

module.exports = new FightersService();