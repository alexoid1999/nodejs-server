import { post, put } from "../requestHelper";

const entity = 'fights';

export const createFightLog = async (userId, body) => {
    return await post(entity, body, userId);
}

export const sendFightLog = async (userId, body) => {
    return await put(entity, userId, body);
}