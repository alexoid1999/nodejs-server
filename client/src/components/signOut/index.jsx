import { unsetLoginSession } from "../../services/authService";
import React from 'react';
import './signOut.css';
import { Button } from '@material-ui/core';

export default function SignOut({ isSignedIn, onSignOut}) {
    const signOut = () => {
        unsetLoginSession();
        onSignOut();
    }

    if(isSignedIn) {
        return (
            <Button onClick={signOut} id="sign-out" variant="contained" color="secondary">Sign out</Button>
        )
    }

    return null;
}