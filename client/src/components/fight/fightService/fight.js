import { controls } from '../../../constants/controls';
import { createElement, getElement } from '../../helpers/domHelper';

export async function fight(firstFighter, secondFighter, onCallUpdateLog) {
  // Создаём независимую копию массива бойцов для защиты Map от мутаций
  const fightersCopy = JSON.parse(JSON.stringify([firstFighter, secondFighter]));
  // Создаём хранилище состояния блока бойцов
  let fighterBlocks = [false, false];
  // Создаём сеты для супер-ударов
  let firstCritical = new Set(), secondCritical = new Set();
  // Для проверки, прошло ли 10 секунд
  let canIUseCritical = [true, true];

  //Получаем ссылку индикаторов здоровья
  const firstFighterHealthBar = getElement({querySelector:'#left-fighter-indicator'});
  const secondFighterHealthBar = getElement({querySelector:'#right-fighter-indicator'});

  
  const arenaBattlefield = getElement({querySelector:'.arena___battlefield', attributes:{style:'justify-content:space-around;'}});
  const arenaFightStatus = getElement({querySelector:'.arena___fight-status'});

  // Получаем ссылку на картинки бойцов и поле битвы для создания простой анимации
  const firstFighterSprite = getElement({querySelector:'.arena___left-fighter', attributes:{style:'position: relative; border-radius:200px;'}});
  const secondFighterSprite = getElement({querySelector:'.arena___right-fighter', attributes:{style:'position: relative; border-radius:200px;'}});


  // Создаём индикаторы критического урона
  const firstFighterCriticalBar = createElement({ tagName: 'div', className: 'arena___left-critical-bar', attributes:{style:'width: 0%'}});
  const secondFighterCriticalBar = createElement({ tagName: 'div', className: 'arena___right-critical-bar', attributes:{style:'width: 0%'}});
  firstFighterHealthBar.appendChild(firstFighterCriticalBar);
  secondFighterHealthBar.appendChild(secondFighterCriticalBar);

  // Создаём логгер событий на поле битвы
  const fightStatusLogger = createElement({tagName: 'div', className:'arena___fight-status-logger'});
  arenaFightStatus.appendChild(fightStatusLogger)

  let firstFighterTimer = setInterval(()=>{
    // Получаем значение "заряда" крит-удара
    let prevState = Number(firstFighterCriticalBar.style.width.replace('%',''));
    // Каждую секунду очищаем сет, чтобы он не запоминал нажатые крит-клавиши
    // Требуется нажать все три в течении 0.5 сек
    firstCritical.clear();
    arenaBattlefield.style.justifyContent='space-around';

    if(prevState<100){
      firstFighterCriticalBar.style.width = prevState + 5 +'%';
      canIUseCritical[0] = false
    }else if(prevState==100){
      canIUseCritical[0] = true
    }
  },500)

  let secondFighterTimer = setInterval(()=>{
    let prevState = Number(secondFighterCriticalBar.style.width.replace('%',''));
    secondCritical.clear();
    if(prevState<100){
      secondFighterCriticalBar.style.width = prevState + 5 +'%';
      canIUseCritical[1] = false;
    }else if(prevState==100){
      canIUseCritical[1] = true
    }
  },500)

  fightLog(fightStatusLogger, 'Fight!', {fighter1Shot: 0, fighter2Shot:0});
  
  return new Promise((resolve) => {
    // Добавляем события клавиатуры
    document.onkeydown = 
    document.onkeyup  = (event)=>{
      switch (event.type){
        case 'keydown':{
          //Если первый боец хочет атаковать
          if(!event.repeat && event.code === controls.PlayerOneAttack){
            // Но у него стоит блок, то ошибка
            if(fighterBlocks[0]) {
              let message = `${fightersCopy[0].name} Can't attack when block`;
              fightLog(fightStatusLogger, message, {fighter1Shot: 0, fighter2Shot:0})
            }
            else{
              // Иначе если у первого блок снят, но у второго блок
              //  то блокируем урон
              if(fighterBlocks[1]) {
                let message = `${fightersCopy[1].name} blocked`;
                fightLog(fightStatusLogger, message, {fighter1Shot: 0, fighter2Shot:0});
                arenaBattlefield.style.justifyContent='flex-end';
              }
              else {
                // Если и у первого и у второго бойца блок не стоит
                // то регистрируем полный урон
                let damage = getDamage(fightersCopy[0],fightersCopy[1]);
                fightersCopy[1].health -= damage;

                arenaBattlefield.style.justifyContent='flex-end';

                let message = `${fightersCopy[0].name} punch on ${damage.toFixed(2)}`;
                fightLog(fightStatusLogger, message, {fighter1Shot: damage.toFixed(2), fighter2Shot:0});
              }
            }    
          }
          //Если второй боец хочет атаковать
          if(!event.repeat && event.code === controls.PlayerTwoAttack){
            if(fighterBlocks[1]) {
              let message = `${fightersCopy[1].name} Can't attack when block`;
              fightLog(fightStatusLogger, message, {fighter1Shot: 0, fighter2Shot:0})
            }
            else{
              if(fighterBlocks[0]) {
                let message = `${fightersCopy[0].name} blocked`;
                fightLog(fightStatusLogger, message, {fighter1Shot: 0, fighter2Shot:0});
                arenaBattlefield.style.justifyContent='flex-start';
              }
              else {
                let damage = getDamage(fightersCopy[1],fightersCopy[0]);
                fightersCopy[0].health -= damage;

                arenaBattlefield.style.justifyContent='flex-start';
                let message = `${fightersCopy[1].name} punch on ${damage.toFixed(2)}`;
                fightLog(fightStatusLogger, message, {fighter1Shot: damage.toFixed(2), fighter2Shot:0});
              }
            }    
          }

          if(!event.repeat 
              && controls.PlayerOneCriticalHitCombination.includes(event.code)
              && !firstCritical.has(event.code)
            ){
            //Если первый игрок единожды нажал на одну из кнопок критической атаки
            // И до этого её не нажимал, то добавляем в сет
            firstCritical.add(event.code)
          }

          if(!event.repeat 
            && controls.PlayerTwoCriticalHitCombination.includes(event.code)
            && !secondCritical.has(event.code)
            ){
            secondCritical.add(event.code)
          }

          if(controls.PlayerOneCriticalHitCombination.every(key=> firstCritical.has(key))
            && canIUseCritical[0]
            ){
            // Если Сет нажатых кнопок содержит все клавиши из критической комбинации
            // и шкала заряда достигла 100
            //  то регистрируем полный удвоенный урон
            // очищаем сет и обнуляем счётчик
              let criticalDamage = getHitPower(fightersCopy[0])*2;
              fightersCopy[1].health -= criticalDamage;

              let message = `${fightersCopy[0].name} use Critical Damage!`;
              fightLog(fightStatusLogger, message, {fighter1Shot: criticalDamage.toFixed(2), fighter2Shot:0});

              firstCritical.clear();
              firstFighterCriticalBar.style.width = '0%';
              arenaBattlefield.style.justifyContent='flex-end';
          }

          if(controls.PlayerTwoCriticalHitCombination.every(key=> secondCritical.has(key))
            && canIUseCritical[1]
            ){
              let criticalDamage = getHitPower(fightersCopy[1])*2;
              fightersCopy[0].health -= criticalDamage;

              let message = `${fightersCopy[1].name} use Critical Damage!`;
              fightLog(fightStatusLogger, message, {fighter1Shot: 0, fighter2Shot: criticalDamage.toFixed(2)});

              firstCritical.clear();
              firstFighterCriticalBar.style.width = '0%';
              arenaBattlefield.style.justifyContent='flex-end';
          }

          // Включаем блок бойца и удерживаем, пока нажата клавиша
          // Меняем значение только единожды для экономии ресурсов на проверке
          if(event.code === controls.PlayerOneBlock && fighterBlocks[0] == false){
            fighterBlocks[0] = true;
            firstFighterSprite.style.background='rgba(50,50,255,0.5)';
          }
          if(event.code === controls.PlayerTwoBlock && fighterBlocks[1] == false){
            fighterBlocks[1] = true;
            secondFighterSprite.style.background='rgba(50,50,255,0.5)';
          }
          break;
        }
        case 'keyup':{
          // Как только кнопка блока отпущена - снять блок
          if(event.code === controls.PlayerOneBlock){
            fighterBlocks[0] = false
            firstFighterSprite.style.background='transparent';
          }
          if(event.code === controls.PlayerTwoBlock){
            fighterBlocks[1] = false;
            secondFighterSprite.style.background='transparent';
          }
          break;
        }
      }

      // Если здоровье одного из бойцов достигло нуля
      if(fightersCopy[0].health<=0) {
        // Сбрасываем прослушки по окончанию
        document.onkeydown = document.onkeyup = document.onkeypress = null;
        resolve(fightersCopy[1]);
      }
      else if (fightersCopy[1].health<=0) {
        // Сбрасываем прослушки по окончанию
        document.onkeydown = document.onkeyup = document.onkeypress = null;
        resolve(fightersCopy[0]);
      }
      else {
        //Иначе - обновляем полоски жизней по формуле 100*(текущая жизнь бойца/начальная жизнь бойца)
        firstFighterHealthBar.style.width =`${fightersCopy[0].health/firstFighter.health*100}%`;
        secondFighterHealthBar.style.width =`${fightersCopy[1].health/secondFighter.health*100}%`;;
      }
    }
  })
  
  function fightLog(loggerElement, text, dataToSend){
    const logRow = createElement({ tagName: 'p', className: 'arena___fight-status-logger-text'});
    logRow.textContent = `${loggerElement.childElementCount} :--> ${text}`;
    loggerElement.prepend(logRow);

    dataToSend.message = text;
    dataToSend.fighter1Health = fightersCopy[0].health>0? fightersCopy[0].health : 0;
    dataToSend.fighter2Health = fightersCopy[1].health>0? fightersCopy[1].health : 0;
    onCallUpdateLog(dataToSend)
  }
}

export function getDamage(attacker, defender) {
  let hit = getHitPower(attacker);
  let block = getBlockPower(defender);
  let damage = hit>block? hit-block : 0;
  return damage
  // return damage
}

export function getHitPower(fighter) {
  let hitPower = fighter.power * (Math.random()*(2-1)+1);
  return hitPower
  // return hit power
}

export function getBlockPower(fighter) {
  // return block power
  return fighter.defense * (Math.random()*(2-1)+1)
}