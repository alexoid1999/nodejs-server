import React from 'react';

import { getFighters } from '../../services/domainRequest/fightersRequest';
import { getUserId } from '../../services/authService';
import { createFightLog } from '../../services/domainRequest/fightRequest';

import versus from '../../resources/versus.png';

import renderArena from './fightService/arena';
import NewFighter from '../newFighter';
import Fighter from '../fighter';
import { Button } from '@material-ui/core';

import './fight.css'

class Fight extends React.Component {
    state = {
        fighters: [],
        fighter1: null,
        fighter2: null,
        visibility: false
    };

    async componentDidMount() {
        const fighters = await getFighters();
        if(fighters && !fighters.error) {
            this.setState({ fighters });
        }
    }

    onFightStart = async() => {
        let currentUserId = await getUserId();
        let createFightStatus = await createFightLog(currentUserId,{fighter1:this.state.fighter1.id, fighter2:this.state.fighter2.id});
        if(createFightStatus && !createFightStatus.error) {
            renderArena(currentUserId, createFightStatus.id, [this.state.fighter1,this.state.fighter2])
        }
    }

    onCreate = (fighter) => {
        this.setState({ fighters: [...this.state.fighters, fighter] });
    }

    onFighter1Select = (fighter1) => {
        this.setState({fighter1 });
    }

    onFighter2Select = (fighter2) => {
        this.setState({ fighter2 });
    }

    getFighter1List = () => {
        const { fighter2, fighters } = this.state;
        if(!fighter2) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter2.id);
    }

    getFighter2List = () => {
        const { fighter1, fighters } = this.state;
        if(!fighter1) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter1.id);
    }

    setVisibility = (visibility)=>{
        this.setState({visibility})
    }

    render() {
        const  { fighter1, fighter2, visibility } = this.state;
        return (
            <div className="fighter-preview___root" id="wrapper">
                <NewFighter visibility={visibility} onCreated={this.onCreate} onSetVisible={this.setVisibility}/>
                {!visibility ? 
                    <div className="preview-container___root" id="figh-wrapper">
                        <Fighter position="left" selectedFighter={fighter1} onFighterSelect={this.onFighter1Select} fightersList={this.getFighter1List() || []} />
                        <div className="preview-container___versus-block btn-wrapper">
                            <img className="preview-container___versus-img" src={versus} alt="Versus"/>
                            <Button onClick={this.onFightStart} variant="contained" color="primary">Start Fight</Button>
                        </div>
                        <Fighter position="right" selectedFighter={fighter2} onFighterSelect={this.onFighter2Select} fightersList={this.getFighter2List() || []} />
                    </div>
                :<div className="preview-container___root" id="figh-wrapper" onClick={()=>this.setVisibility(!visibility)}>
                    <div>Select Fighters</div>
                </div>
                }
                
            </div>
        );
    }
}

export default Fight;