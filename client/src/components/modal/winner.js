import {showModal} from './modal';
import {createFighterImage} from '../fighterPreview';
import { createElement } from '../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function 
  let bodyElement = createElement({tagName:'div', className:'modal-body'});

  bodyElement.appendChild(createFighterImage(fighter));

  let onClose = async()=>{
    window.location.reload()
  }

  return showModal({title:`The winner is ${fighter.name}`, bodyElement, onClose})
}
