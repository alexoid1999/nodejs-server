import { createElement } from './helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  fighterElement.appendChild(createFighterImage(fighter));
  fighterElement.appendChild(createFighterDescription(fighter, positionClassName));
    return fighterElement;
}

function createFighterDescription(fighter, position){
  let newDescriptionBlock = createElement({
    tagName: 'div',
    className: `fighter-preview___text-block ${position}`
  });

  Object.keys(fighter).forEach(key=>{
    if(key ==='source' || key === 'id') {}
    else {newDescriptionBlock.appendChild(addText(key, fighter[key]))}
  })
  return newDescriptionBlock
}

function addText(key, description){
  let newDescription = createElement({
    tagName: 'p',
    className: 'fighter-preview___text'
  });
  newDescription.textContent = key[0].toUpperCase()+ key.slice(1) + ': '+description

  return newDescription
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
