import React, { useState } from 'react';
import { FormControl, InputLabel, makeStyles, Select } from '@material-ui/core';
import { MenuItem } from 'material-ui';

import {skins} from '../../constants/skins.js';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

export default function Fighter({ position, fightersList, onFighterSelect, selectedFighter }) {
    const classes = useStyles();
    const [fighter, setFighter] = useState();

    const handleChange = (event) => {
        debugger;
        setFighter(Object.assign(event.target.value, {source:skins[Math.floor(Math.random()*(6-1+1)+1)]}));
        onFighterSelect(event.target.value);
    };

    return (
        <div className={position==="left"? "fighter-preview___left":"fighter-preview___right"}>
            <FormControl className={classes.formControl}>
                <InputLabel id="simple-select-label">Select Fighter</InputLabel>
                <Select
                    labelId="simple-select-label"
                    id="simple-select"
                    value={fighter}
                    onChange={handleChange}
                >
                    {fightersList.map((it, index) => {
                        return (
                            <MenuItem key={`${index}`} value={it}>{it.name}</MenuItem>
                        );
                    })}
                </Select>
                {selectedFighter
                    ? <div style={{display:'inline-flex', flexDirection:position==="left"?'row-reverse':'row'}}>
                        <img src={selectedFighter.source} alt={selectedFighter.name}/>
                        <div className={position==="left"? "fighter-preview___text-block-left":"fighter-preview___text-block-right"}>
                            <div className={position==="left"?"fighter-preview___text-left":"fighter-preview___text-right"}>Name: {selectedFighter.name}</div>
                            <div className={position==="left"?"fighter-preview___text-left":"fighter-preview___text-right"}>Power: {selectedFighter.power}</div>
                            <div className={position==="left"?"fighter-preview___text-left":"fighter-preview___text-right"}>Defense: {selectedFighter.defense}</div>
                            <div className={position==="left"?"fighter-preview___text-left":"fighter-preview___text-right"}>Health: {selectedFighter.health}</div>
                        </div>
                    </div>
                    : null
                }
            </FormControl>
        </div>)
}
