exports.fight = {
    "id": /[a-zA-Z0-9]{8}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{12}/,
    "fighter1": /[a-zA-Z0-9]{8}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{12}/,
    "fighter2": /[a-zA-Z0-9]{8}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{12}/,
    "log": [
        {
            "message": /[a-zA-Z0-9]{1,}/,
            "fighter1Shot": /^\d{1,}/, //любое число больше нуля
            "fighter2Shot": /^\d{1,}/,
            "fighter1Health": /^\d{1,}/,
            "fighter2Health": /^\d{1,}/
        }
    ]
}