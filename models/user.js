exports.user = {
    id: /[a-zA-Z0-9]{8}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{12}/,
    firstName: /[a-zA-Z]/,
    lastName: /[a-zA-Z]/,
    email: /[a-zA-Z0-9]+@gmail+\.com/,
    phoneNumber: /\+380+[0-9]{9}$/,
    password: /(?=^.{3,}$)(?=.*[a-zA-Z0-9])(?!.*\s).*$/ // min 3 symbols
}