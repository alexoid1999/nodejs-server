
const { fight } = require('../models/fight');

const createFightValid = (req, res, next) => {

    const requestedFight = req.body;
    const requestedFightKeys = Object.keys(requestedFight);
    const fightModelKeys = Object.keys(fight).filter(key=> key!=='id' && key!=='log');

    // Сравниваем ключи модели и полученного объекта
    const validKeys = fightModelKeys.filter(key=> requestedFightKeys.includes(key));
    const invalidKeys = requestedFightKeys.filter(key=> !fightModelKeys.includes(key));
    // Если каждый ключ модели и каждое значение по ключу совпадают с полученным объектом
    if(validKeys.length===fightModelKeys.length && invalidKeys.length === 0
        && requestedFightKeys
        .every(regModelKey=> fight[regModelKey].test(requestedFight[regModelKey]))
        ){
            next();
    }
    else if(validKeys.length<fightModelKeys.length){
        res.err = 'User creating is invalid. Forgotten keys: '+ fightModelKeys.filter(key=>!validKeys.includes(key)).toString();
    }
    else if(invalidKeys.length!==0){
        res.err = 'Fight updating is invalid. Invalid keys: '+ invalidKeys.toString();
    } 
    else{
        let invalidFields=``;
        fightModelKeys.forEach(regModelKey => {
            if(!fight[regModelKey].test(requestedFight[regModelKey])){
                invalidFields+=`${regModelKey}: ${requestedFight[regModelKey]} `
            }
        });
        res.err = 'Fight updating is invalid. Invalid values: '+ invalidFields;
    }
    next();
}

const updateFightValid = (req, res, next) => {

    const requestedFight = req.body;
    const modelLog = fight.log[0];
    const requestedFightKeys = Object.keys(requestedFight);
    const fightModelKeys = Object.keys(fight).filter(key=> key=='id' || key=='log');
    const fightLogModelKeys = Object.keys(modelLog);

    // Сравниваем ключи модели и полученного объекта
    const validKeys = fightModelKeys.filter(key=> requestedFightKeys.includes(key));
    const invalidKeys = requestedFightKeys.filter(key=> !fightModelKeys.includes(key));
    // Если каждый ключ модели совпадает
    // Спускаемся на уровень ниже и проверяем каждый ключ объекта log  

    if(validKeys.length===fightModelKeys.length && invalidKeys.length === 0 
        && fight["id"].test(requestedFight["id"])
        ){
        // Получаем ключи параметра log из реквеста
        const requestedLogFightKeys = Object.keys(requestedFight.log);
        // Сравниваем ключи log модели и полученного объекта
        const validLogKeys = fightLogModelKeys.filter(key=> requestedLogFightKeys.includes(key));
        const invalidLogKeys = requestedLogFightKeys.filter(key=> !fightLogModelKeys.includes(key));
        
        // Если внутри fight.log все ключи валидны и нет невалидных
        if(validLogKeys.length===fightLogModelKeys.length
            && invalidLogKeys.length === 0 
            && requestedLogFightKeys
            .every(logKey=> modelLog[logKey].test(requestedFight.log[logKey]))
            ){ 
                next();
            }
        else if(validLogKeys.length<fightLogModelKeys.length){
            res.err = 'User creating is invalid. Forgotten keys: '+ fightLogModelKeys.filter(key=>!validLogKeys.includes(key)).toString();
        }
        // Иначе если внутри fight.log найден невалидный ключ
        else if(invalidLogKeys.length!==0){
            res.err = 'Fight log is invalid. Invalid keys: '+ invalidLogKeys.toString();
        } 
        // Или невалидное значение
        else{
            let invalidFields=``;
            fightLogModelKeys.forEach(reglogModelKey => {
                if(!modelLog[reglogModelKey].test(requestedFight.log[reglogModelKey])){
                    invalidFields+=`${reglogModelKey}: ${requestedFight.log[reglogModelKey]} `
                }
            });
            res.err = 'Fight log is invalid. Invalid values: '+ invalidFields;
        }
    }
    else if(validKeys.length<fightModelKeys.length){
        res.err = 'Fight updating is invalid. Forgotten keys: '+ fightModelKeys.filter(key=>!validKeys.includes(key)).toString();
    }
    else if(invalidKeys.length!==0){
        res.err = 'Fight updating is invalid. Invalid keys: '+ invalidKeys.toString();
    } 
    else{
        let invalidFields=``;
        fightModelKeys.forEach(regModelKey => {
            if(!fight[regModelKey].test(requestedFight[regModelKey])){
                invalidFields+=`${regModelKey}: ${requestedFight[regModelKey]} `
            }
        });
        res.err = 'Fight updating is invalid. Invalid values: '+ invalidFields;
    }
    next();
}

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;