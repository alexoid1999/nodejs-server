const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    // Получаем объект из реквеста
    const requestedUser = req.body;

    // Получаем список ключей из эталонной Модели без айди и список ключей из объекта-реквеста
    const requestedUserKeys = Object.keys(requestedUser);
    const userModelKeys = Object.keys(user).filter(key=>key!=='id');

    // Сравниваем ключи модели и полученного объекта
    const validKeys = userModelKeys.filter(key=> requestedUserKeys.includes(key));
    const invalidKeys = requestedUserKeys.filter(key=> !userModelKeys.includes(key));
    // Если каждый ключ модели и каждое значение по ключу совпадают с полученным объектом
    if(validKeys.length===userModelKeys.length && invalidKeys.length === 0
        && userModelKeys
        .every(regModelKey=> user[regModelKey].test(requestedUser[regModelKey]))
        ){
        next();
    }
    // Иначе если пропущено необходимое поле
    else if(validKeys.length<userModelKeys.length){
        res.err = 'User creating is invalid. Forgotten keys: '+ userModelKeys.filter(key=>!validKeys.includes(key)).toString();
    }
    // Иначе если найдены недопустимые ключи
    else if(invalidKeys.length!==0){
        res.err = 'User create is invalid. Invalid keys: '+ invalidKeys.toString();
    } 
    // Иначе если найдены недопустимые значения полей или не все поля заполнены
    else{
        let invalidFields=``;
        userModelKeys.forEach(regModelKey => {
            if(!user[regModelKey].test(requestedUser[regModelKey])){
                invalidFields+=`${regModelKey}: ${requestedUser[regModelKey]} `
            }
        });
        res.err = 'User creating is invalid. Invalid values: '+ invalidFields;
    }
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const requestedUser = req.body;
    const requestedUserKeys = Object.keys(requestedUser);
    const updatedUserModelKeys = Object.keys(user).filter(key=>key!=='id');

    // Сравниваем ключи модели и полученного объекта
    const validKeys = updatedUserModelKeys.filter(key=> requestedUserKeys.includes(key));
    const invalidKeys = requestedUserKeys.filter(key=> !updatedUserModelKeys.includes(key));
    // Если каждый ключ модели и каждое значение по ключу совпадают с полученным объектом
    if(validKeys.length>0 && invalidKeys.length === 0
        && requestedUserKeys
        .every(regModelKey=> user[regModelKey].test(requestedUser[regModelKey]))
        ){
        next();
    }
    else if(invalidKeys.length!==0){
        res.err = 'User updating is invalid. Invalid keys: '+ invalidKeys.toString();
    } 
    else{
        let invalidFields=``;
        updatedUserModelKeys.forEach(regModelKey => {
            if(!user[regModelKey].test(requestedUser[regModelKey])){
                invalidFields+=`${regModelKey}: ${requestedUser[regModelKey]} `
            }
        });
        res.err = 'User updating is invalid. Invalid values: '+ invalidFields;
    }
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;