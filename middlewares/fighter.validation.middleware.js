const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
     // Получаем объект из реквеста
     const requestedFighter = req.body;

     // Если поле health отсутствует в реквесте, то ставим значение по умолчанию
     if(!requestedFighter.health) requestedFighter.health = 100;

     // Получаем список ключей из эталонной Модели без айди и список ключей из объекта-реквеста
     const requestedFighterKeys = Object.keys(requestedFighter);
     const fighterModelKeys = Object.keys(fighter).filter(key=>key!=='id');
 
     // Сравниваем ключи модели и полученного объекта
     const validKeys = fighterModelKeys.filter(key=> requestedFighterKeys.includes(key));
     const invalidKeys = requestedFighterKeys.filter(key=> !fighterModelKeys.includes(key));
     // Если каждый ключ модели и каждое значение по ключу совпадают с полученным объектом
     if(validKeys.length===fighterModelKeys.length && invalidKeys.length === 0
         && fighterModelKeys
         .every(regModelKey=> fighter[regModelKey].test(requestedFighter[regModelKey]))
         ){
        next();   
     }
     else if(validKeys.length<fighterModelKeys.length){
        res.err = 'User creating is invalid. Forgotten keys: '+ fighterModelKeys.filter(key=>!validKeys.includes(key)).toString();
     }
     // Иначе если найдены недопустимые ключи
     else if(invalidKeys.length!==0){
         res.err = 'Fighter create is invalid. Invalid keys: '+ invalidKeys.toString();
     } 
     // Иначе если найдены недопустимые значения полей или не все поля заполнены
     else{
         let invalidFields=``;
         fighterModelKeys.forEach(regModelKey => {
             if(!fighter[regModelKey].test(requestedFighter[regModelKey])){
                 invalidFields+=`${regModelKey}: ${requestedFighter[regModelKey]} `
             }
         });
         res.err = 'Fighter creating is invalid. Invalid values: '+ invalidFields;
     }
     next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const requestedFighter = req.body;
    const requestedFighterKeys = Object.keys(requestedFighter);
    const updatedFighterModelKeys = Object.keys(fighter).filter(key=>key!=='id');

    // Сравниваем ключи модели и полученного объекта
    const validKeys = updatedFighterModelKeys.filter(key=> requestedFighterKeys.includes(key));
    const invalidKeys = requestedFighterKeys.filter(key=> !updatedFighterModelKeys.includes(key));
    // Если каждый ключ модели и каждое значение по ключу совпадают с полученным объектом
    if(validKeys.length>0 && invalidKeys.length === 0
        && requestedFighterKeys
        .every(regModelKey=> fighter[regModelKey].test(requestedFighter[regModelKey]))
        ){
            next();
    }
    else if(invalidKeys.length!==0){
        res.err = 'Fighter updating is invalid. Invalid keys: '+ invalidKeys.toString();
    } 
    else{
        let invalidFields=``;
        updatedFighterModelKeys.forEach(regModelKey => {
            if(!fighter[regModelKey].test(requestedFighter[regModelKey])){
                invalidFields+=`${regModelKey}: ${requestedFighter[regModelKey]} `
            }
        });
        res.err = 'Fighter updating is invalid. Invalid values: '+ invalidFields;
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;