const responseMiddleware = (req, res, next) => {
    // Если прошлые функции оставили что-то в res.err, то останавливаем цикл и отправляем ошибку
    if(res.err && res.err!==''){
        res.status(400).json({error: true, message:res.err})
    }
    else if(res.notFound && res.notFound!==''){
        res.status(404).json({ error:true, message:res.notFound})
    }
    // Иначе если прошлые функции оставили что-то в res.data, то останавливаем цикл и отправляем результат
    else if(res.data){
        res.status(200).json(res.data);
    }
    // Иначе просто продолжаем цикл
    else next()  
   // TODO: Implement middleware that returns result of the query
}

exports.responseMiddleware = responseMiddleware;